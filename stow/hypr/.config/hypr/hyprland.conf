monitor=,preferred,auto,auto

exec-once = waybar
exec-once = mako
exec-once = makoctl mode -s hypr
exec-once = avizo-service
exec-once = wlsunset -l 50 -L 30
exec-once = swaybg -i ~/documents/wallpapers/landscape__12____japanese_pagoda_by_ncoll36_db9snou.jpg -m fill
$lock = swaylock --daemonize --color 000000
exec-once = swayidle -w timeout 300 '$lock' timeout 360 'hyprctl dispatch "dpms off"' resume 'hyprctl dispatch "dpms on"' before-sleep '$lock'

# source = ~/.config/hypr/myColors.conf

input {
    kb_layout = us,ua
    kb_variant =
    kb_model =
    kb_options = grp:alt_shift_toggle
    kb_rules =

    follow_mouse = 1

    touchpad {
        natural_scroll = true
        disable_while_typing = false
    }

    sensitivity = 0 # -1.0 - 1.0, 0 means no modification.
}

general {
    gaps_in = 3
    gaps_out = 3
    border_size = 2
    col.active_border = rgb(383a42)
    col.inactive_border = rgba(00000000)

    layout = master
}

decoration {
    #rounding = 10

    blur {
        enabled = true
        size = 2
        passes = 1
        new_optimizations = true
        ignore_opacity = true
        xray = true
        noise = 0
    }

    drop_shadow = true
    shadow_range = 10
    shadow_render_power = 2
    col.shadow = rgba(1a1a1a3e)
}

animations {
    enabled = true

    animation = windows, 1, 5, default, popin 80%
    animation = windowsOut, 1, 5, default, popin 80%
    animation = border, 1, 5, default
    animation = borderangle, 1, 10, default
    animation = fade, 1, 7, default
    animation = workspaces, 1, 4, default
    # layerrule = noanim, ^menu$
}

misc {
    disable_hyprland_logo = true
    disable_splash_rendering = true

    enable_swallow = true
    # swallow_regex = ^kitty$
    # swallow_exception_regex = ^zenity$
}

master {
    new_is_master = false
    no_gaps_when_only = true
    mfact = 0.575  # master split factor
}

gestures {
    workspace_swipe = false
}

$mod = SUPER
$menu = bemenu-configured


# Basic controls

# Move focus with mod + Vim keys
bind = $mod, H, movefocus, l
bind = $mod, J, movefocus, d
bind = $mod, K, movefocus, u
bind = $mod, L, movefocus, r

# Move focused window with mod + Shift + Vim keys
bind = $mod + SHIFT, H, movewindow, l
bind = $mod + SHIFT, J, movewindow, d
bind = $mod + SHIFT, K, movewindow, u
bind = $mod + SHIFT, L, movewindow, r

bind = $mod, G, togglegroup
bind = $mod + SHIFT, G, changegroupactive

# Switch workspaces with mod + [0-9]
bind = $mod, 1, workspace, 1
bind = $mod, 2, workspace, 2
bind = $mod, 3, workspace, 3
bind = $mod, 4, workspace, 4
bind = $mod, 5, workspace, 5
bind = $mod, 6, workspace, 6
bind = $mod, 7, workspace, 7
bind = $mod, 8, workspace, 8
bind = $mod, 9, workspace, 9
bind = $mod, 0, workspace, 10

# Move active window to a workspace with mod + SHIFT + [0-9]
bind = $mod + SHIFT, 1, movetoworkspacesilent, 1
bind = $mod + SHIFT, 2, movetoworkspacesilent, 2
bind = $mod + SHIFT, 3, movetoworkspacesilent, 3
bind = $mod + SHIFT, 4, movetoworkspacesilent, 4
bind = $mod + SHIFT, 5, movetoworkspacesilent, 5
bind = $mod + SHIFT, 6, movetoworkspacesilent, 6
bind = $mod + SHIFT, 7, movetoworkspacesilent, 7
bind = $mod + SHIFT, 8, movetoworkspacesilent, 8
bind = $mod + SHIFT, 9, movetoworkspacesilent, 9
bind = $mod + SHIFT, 0, movetoworkspacesilent, 10

# Scroll through existing workspaces with mod + scroll
bind = $mod, mouse_down, workspace, e+1
bind = $mod, mouse_up, workspace, e-1

# Move/resize windows with mod + LMB/RMB and dragging
bindm = $mod, mouse:272, movewindow
bindm = $mod, mouse:273, resizewindow

bind = $mod + SHIFT, E, exit,
# binde = $mod + SHIFT, Q, killactive,
bind = $mod + SHIFT, Q, killactive,
bind = $mod + SHIFT, SPACE, togglefloating,
bind = $mod, F, fullscreen,
bind = $mod + SHIFT, F, fullscreen, 1
bind = $mod, P, pseudo, # dwindle


# Other controls

bind = $mod, Return, exec, $TERMINAL
# bind = $mod, T, exec, hypr-touch-toggle
bind = $mod, Space, exec, hypr-touch-toggle
bind = , KP_Divide, exec, hypr-touch-toggle
bind = $mod, D, exec, j4-dmenu-desktop --no-generic --dmenu="$menu -p Запустити" --term="$TERMINAL"
bind = $mod, Z, pass, ^com\.obsproject\.Studio$
bind = $mod, X, pass, ^com\.obsproject\.Studio$
bind = , Print, exec, grim-screen
bind = SHIFT, Print, exec, grim-region
bind = $mod, Print, exec, grim-hypr-window
bind = $mod + SHIFT, L, exec, $lock

bind = $mod, KP_Home, exec, $TERMINAL lf
bind = $mod, KP_Up, exec, $TERMINAL lf
bind = $mod, KP_Prior, exec, $TERMINAL tremc -X
# KP_Left
bind = $mod, KP_Begin, exec, $TERMINAL tg
bind = $mod, KP_Right, exec, torbrowser-launcher
bind = $mod, KP_End, exec, $BROWSER
bind = $mod, KP_Down, exec, lbt-feed-menu
bind = $mod, KP_Next, exec, $TERMINAL newsboat

bind = $mod + ALT, W, exec, iwctl-menu
bind = $mod + ALT, E, exec, emoji-menu
bind = $mod + ALT, Y, exec, $TERMINAL video-clipboard-open
bind = $mod + ALT, I, exec, iwctl-restart
bind = $mod + ALT, P, exec, passmenu
bind = $mod + ALT, A, exec, $TERMINAL pulsemixer
bind = $mod + ALT, C, exec, $TERMINAL $EDITOR .config/hypr/hyprland.conf
bind = $mod + ALT, N, exec, makoctl dismiss
bind = $mod + ALT, M, exec, $TERMINAL htop


# Using Avizo
bind = ,XF86MonBrightnessUp,   exec, lightctl up
bind = ,XF86MonBrightnessDown, exec, lightctl down
bind = ,XF86AudioRaiseVolume,  exec, volumectl -u up 2
bind = ,XF86AudioLowerVolume,  exec, volumectl -u down 2
bind = ,XF86AudioMute,         exec, volumectl toggle-mute
bind = ,XF86AudioMicMute,      exec, volumectl -m toggle-mute
bind = $mod, m,                exec, volumectl -m toggle-mute

# Hacks
bind = $mod + ALT, F, resizeactive, exact 100% 100%


# Window rules

# common size for web view area to maximize privacy, plus space for tab bar
windowrule = float, Tor Browser$
windowrule = minsize 1200 687, Tor Browser$

windowrule = float, .exe$
windowrule = rounding 0, .exe$
windowrule = noborder, .exe$

windowrule = float, .tmp$
windowrule = rounding 0, .tmp$
windowrule = noborder, .tmp$

# uxn11
windowrule = float, uxn11
windowrule = float, title:^(\w*).rom$

windowrule = float, ^zenity$

# windowrulev2 = windowdance, workspace:1
# windowrulev2 = float, workspace:1
# windowrulev2 = minsize 1366 768, workspace:1
# windowrulev2 = opaque, workspace:1
