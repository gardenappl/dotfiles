-- Add additional capabilities supported by nvim-cmp
local capabilities = require("cmp_nvim_lsp").default_capabilities()

local lspconfig = require('lspconfig')

-- Enable some language servers with the additional completion capabilities offered by nvim-cmp
local servers = {
	'bashls',
	'clangd',
	'lua_ls',
	'pyright',
	'tsserver'
}
for _, lsp in ipairs(servers) do
	lspconfig[lsp].setup {
		settings = {
			Lua = {
				workspace = {
					-- https://github.com/LuaLS/lua-language-server/issues/783
					checkThirdParty = false
				}
			}
		},
		-- on_attach = my_custom_on_attach,
		capabilities = capabilities
	}
end



-- Global mappings.
-- See `:help vim.diagnostic.*` for documentation on any of the below functions
vim.keymap.set('n', '<leader>e', vim.diagnostic.open_float, { desc = "Show diagnostics in floating window" })
vim.keymap.set('n', '[d', vim.diagnostic.goto_prev, { desc = "Previous diagnostic" })
vim.keymap.set('n', ']d', vim.diagnostic.goto_next, { desc = "Next diagnostic" })
vim.keymap.set('n', '<leader>q', vim.diagnostic.setloclist, { desc = "Add diagnostics to location list" })

-- Use LspAttach autocommand to only map the following keys
-- after the language server attaches to the current buffer
vim.api.nvim_create_autocmd('LspAttach', {
	group = vim.api.nvim_create_augroup('UserLspConfig', {}),
	callback = function(ev)
		-- Enable completion triggered by <c-x><c-o>
		-- vim.bo[ev.buf].omnifunc = 'v:lua.vim.lsp.omnifunc'

		-- Buffer local mappings.
		local set_local = function(mode, lhs, rhs, desc)
			vim.keymap.set(mode, lhs, rhs, { buffer = ev.buf, desc = desc })
		end
		set_local('n', 'gD', vim.lsp.buf.declaration, "Go to declaration")
		set_local('n', 'gd', vim.lsp.buf.definition, "Go to definition")
		set_local('n', 'K', vim.lsp.buf.hover, "Display hover info")
		set_local('n', 'gi', vim.lsp.buf.implementation, "Go to implementation")
		set_local('n', '<C-k>', vim.lsp.buf.signature_help, "Display signature")
		set_local('n', '<leader>wa', vim.lsp.buf.add_workspace_folder, "Add workspace folder")
		set_local('n', '<leader>wr', vim.lsp.buf.remove_workspace_folder, "Remove workspace folder")
		set_local('n', '<leader>wl', function()
			print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
		end, "List workspace folders")
		set_local('n', '<leader>D', vim.lsp.buf.type_definition, "Go to type definition")
		set_local('n', '<leader>rn', vim.lsp.buf.rename, "Rename symbol")
		set_local({ 'n', 'v' }, '<leader>ca', vim.lsp.buf.code_action, "Code action")
		set_local('n', 'gr', vim.lsp.buf.references, "List references")
		set_local('n', '<leader>f', function()
			vim.lsp.buf.format { async = true }
		end, "Format current buffer")
	end,
})
