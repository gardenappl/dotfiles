-- REQUIREMENTS: cmake make python-pynvim

vim.g.loaded_perl_provider = 0
vim.g.loaded_ruby_provider = 0
vim.g.loaded_node_provider = 0
vim.g.loaded_python_provider = 0
vim.g.loaded_python3_provider = 0

local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
	vim.fn.system({
		"git",
		"clone",
		"--filter=blob:none",
		"https://github.com/folke/lazy.nvim.git",
		"--branch=stable", -- latest stable release
		lazypath,
	})
end
vim.opt.rtp:prepend(lazypath)


vim.g.mapleader = ' '
vim.opt.termguicolors = true


require("lazy").setup({
	-- {
	-- 	"navarasu/onedark.nvim",
	-- 	priority = 1000,
	-- 	init = function()
	-- 		require("onedark").setup({
	-- 			style = "light",
	-- 			ending_tildes = true
	-- 		})
	-- 		require("onedark").load()
	-- 	end
	-- },
	{
		"tomasr/molokai",
		config = function()
			vim.cmd [[colorscheme molokai]]
		end
	},
	{
		"folke/which-key.nvim",
		event = "VeryLazy",
		init = function()
			vim.o.timeout = true
			vim.o.timeoutlen = 500
		end,
		opts = {
			window = {
				-- winblend = 50
			}
		}
	},
	{
		"rafamadriz/friendly-snippets",
		lazy = true,
		config = function()
			require("luasnip.loaders.from_vscode").lazy_load()
		end,
	},
	"dstein64/vim-startuptime",
	{
		"L3MON4D3/LuaSnip",
		lazy = true,
		build = "make install_jsregexp",
		dependencies = { "rafamadriz/friendly-snippets" }
	},
	{
		"hrsh7th/nvim-cmp",
		-- event = "VeryLazy",
		dependencies = {
			"hrsh7th/cmp-nvim-lsp",
			"hrsh7th/cmp-buffer",
			"hrsh7th/cmp-path",
			"saadparwaiz1/cmp_luasnip",
			"nvim-lua/plenary.nvim" -- Used in my lua config
		},
		config = function()
			require("minude/completion")
		end
	},
	{
		"folke/neodev.nvim",
		lazy = true,
		opts = {}
	},
	{
		"hrsh7th/cmp-nvim-lsp",
		lazy = true,
		dependencies = { "neovim/nvim-lspconfig" }
	},
	{
		"saadparwaiz1/cmp_luasnip",
		lazy = true,
		dependencies = { "L3MON4D3/LuaSnip" }
	},
	{
		"neovim/nvim-lspconfig",
		lazy = true,
		config = function()
			require("minude/lsp")
		end,
		dependencies = { "folke/neodev.nvim" }
	},
	{
		"nvim-lua/plenary.nvim",
		lazy = true
	},
	"tpope/vim-sleuth",
	{
		"lukas-reineke/indent-blankline.nvim",
		init = function()
			require("ibl").setup()
			-- vim.opt.list = true
			-- vim.opt.listchars:append "space:⋅"
			-- vim.opt.listchars:append "eol:↴"
		end,
		main = "ibl",
		opts = {
			indent = { char = "|" }
		}
	},
	{
		"gelguy/wilder.nvim",
		event = "CmdlineEnter",
		config = function()
			local wilder = require("wilder")
			wilder.setup({ modes = { ':', '/', '?' } })
			-- Disable Python remote plugin
			-- wilder.set_option('use_python_remote_plugin', 0)

			wilder.set_option('pipeline', {
				wilder.branch(
					wilder.python_file_finder_pipeline({
						-- Don't try to find project root, as we use project_nvim
						path = "",
						filters = { "cpsm_filter" }
					}),
					wilder.cmdline_pipeline({
						fuzzy = 1,
						fuzzy_filter = wilder.lua_fzy_filter(),
					}),
					wilder.vim_search_pipeline()
				)
			})

			wilder.set_option('renderer', wilder.renderer_mux({
				[':'] = wilder.popupmenu_renderer({
					highlighter = wilder.lua_fzy_highlighter(),
					left = {
						' ',
						wilder.popupmenu_devicons()
					},
					right = {
						' ',
						wilder.popupmenu_scrollbar()
					},
				}),
				['/'] = wilder.wildmenu_renderer({
					highlighter = wilder.lua_fzy_highlighter(),
				}),
			}))
		end,
		-- build = function()
		-- 	vim.cmd("UpdateRemotePlugins")
		-- end,
		dependencies = {
			"romgrk/fzy-lua-native",
			"nixprime/cpsm"
		}
	},
	{
		"romgrk/fzy-lua-native",
		lazy = true,
		build = "make"
	},
	{
		"nixprime/cpsm",
		lazy = true,
		build = "PY3=ON ./install.sh"
	},
	{
		"numToStr/Comment.nvim",
		event = "VeryLazy",
		config = true
	},
	{
		"ggandor/leap.nvim",
		event = "VeryLazy",
		config = function()
			require('leap').add_default_mappings()

			-- https://github.com/ggandor/leap.nvim/issues/70
			vim.api.nvim_create_autocmd(
				"User",
				{
					callback = function()
						vim.cmd.hi("Cursor", "blend=100")
						vim.opt.guicursor:append { "a:Cursor/lCursor" }
					end,
					pattern = "LeapEnter"
				}
			)
			vim.api.nvim_create_autocmd(
				"User",
				{
					callback = function()
						vim.cmd.hi("Cursor", "blend=0")
						vim.opt.guicursor:remove { "a:Cursor/lCursor" }
					end,
					pattern = "LeapLeave"
				}
			)
		end
	},
	{
		"ahmedkhalf/project.nvim",
		config = function()
			require("project_nvim").setup({
				-- silent_chdir = false,
				ignore_lsp = { "lua_ls" },
				patterns = { "lazy-lock.json", ".git", "_darcs", ".hg", ".bzr", ".svn", "Makefile",
					"package.json" },
			})
		end
	},
	{
		"jiangmiao/auto-pairs",
		-- event = "VeryLazy"
	},
	-- {
	-- 	"windwp/nvim-autopairs",
	-- 	event = "InsertEnter",
	-- 	opts = {},
	-- 	config = function()
	-- 		local cmp_autopairs = require('nvim-autopairs.completion.cmp')
	-- 		local cmp = require('cmp')
	-- 		cmp.event:on(
	-- 			'confirm_done',
	-- 			cmp_autopairs.on_confirm_done()
	-- 		)
	-- 	end,
	-- 	dependencies = {
	-- 		"hrsh7th/nvim-cmp"
	-- 	}
	-- },
	{
		"ray-x/lsp_signature.nvim",
		-- event = "VeryLazy",
		opts = {
			zindex = 50,
			close_timeout = 0,
			hint_enable = false
		}
	},
	{
		"norcalli/nvim-colorizer.lua",
		-- event = "VeryLazy",
		opts = {
			"*",
			css = { css = true, names = true },
			javascript = { css = true, names = true },
			html = { css = true, names = true }
		}
	},
	{
		"nvim-lualine/lualine.nvim",
		opts = {
			sections = {
				lualine_b = { --[[ 'buffers',  ]] 'diagnostics' },
				lualine_x = { "encoding", "filetype" }
			},
			options = {
				section_separators = { left = '', right = '' },
				component_separators = { left = '', right = '' }
			}
		}
	},
	{
		"kdheepak/tabline.nvim",
		opts = {
			enable = true,
			options = {
				show_bufnr = true,
				modified_icon = '+'
			}
		}
	},
	"bellinitte/uxntal.vim"
})

vim.api.nvim_create_autocmd("FileType", {
	pattern = "uxntal",
	callback = function()
		vim.opt.colorcolumn = "72"
		vim.g.AutoPairs = {
			["["] = "]",
			["{"] = "}",
			["("] = ")",
			["?{"] = "}",
			["!{"] = "}"
		}
	end
})


vim.opt.relativenumber = true
vim.opt.number = true

vim.opt.cursorline = true

-- Enable mouse in every mode
vim.opt.mouse = 'a'


vim.g.netrw_winsize = -30
-- Do not display window
vim.g.netrw_banner = 0
-- Simple sorting: directories first, then files
vim.g.netrw_sort_sequence = "[\\/]$,*"
-- Tree style listing
vim.g.netrw_liststyle = 3
-- Open file in current window
vim.g.netrw_browse_split = 4
-- Auto-update content
vim.g.netrw_keepdir = 0

vim.keymap.set('n', "<leader>se", ":setlocal spell! spelllang=en_us<CR>", { desc = "Spellcheck (en_US)" })
vim.keymap.set('n', "<leader>su", ":setlocal spell! spelllang=uk_ua<CR>", { desc = "Spellcheck (uk_UA)" })
vim.keymap.set('n', "<leader>sr", ":setlocal spell! spelllang=ru_ru<CR>", { desc = "Spellcheck (ru_RU)" })

-- Disable case sensitivity, mostly for search
vim.opt.ignorecase = true
vim.opt.smartcase = true
