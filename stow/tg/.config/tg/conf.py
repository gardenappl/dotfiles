import os

def get_pass(key):
    # retrieves key from password store
    return os.popen("pass show {} | head -n 1".format(key)).read().strip()

PHONE = get_pass('social/tg')

NOTIFY_CMD = "notify-send {title} {msg}"
LONG_MSG_CMD = "nvim {file_path}"

DOWNLOAD_DIR = os.path.expanduser("~/downloads/tg")

FILE_PICKER_CMD = "lf -selection-path {file_path}"

# try:
#     with open(os.path.expanduser("~/.config/tg/tdlib_path")) as f:
#         TDLIB_PATH = f.readline().strip()
# except IOError:
#     print("Using default TDLIB_PATH")

URL_VIEW = 'urlscan'
