[`Zon_komt_op_boven_een_winters_landschap._Locatie,_Langweerderwielen_\(Langwarder_Wielen\)_en_omgeving_03.jpg`](https://commons.wikimedia.org/wiki/File:Zon_komt_op_boven_een_winters_landschap._Locatie,_Langweerderwielen_%28Langwarder_Wielen%29_en_omgeving_03.jpg)
* Famberhorst, CC-BY-SA-4.0

[`ubuntu-contest-milky-way-by-pauloup.jpg`](https://discourse.ubuntu.com/t/wallpaper-competition-for-impish-indri-ubuntu-21-10/22852/60)
* Paulo José Oliveira Amaro, CC-BY-SA-4.0

[`2048px-Puesta_de_sol,_Tok,_Alaska,_Estados_Unidos,_2017-08-28,_DD_189-191_HDR.jpg`](https://commons.wikimedia.org/wiki/File:Puesta_de_sol,_Tok,_Alaska,_Estados_Unidos,_2017-08-28,_DD_189-191_HDR.jpg)
* Diego Delso, [delso.photo](https://delso.photo), CC-BY-SA

[`landscape__12____japanese_pagoda_by_ncoll36_db9snou.jpg`](https://www.deviantart.com/ncoll36/art/Landscape-12-Japanese-Pagoda-681581550)
* ncoll36, CC-BY-NC-ND-3.0
