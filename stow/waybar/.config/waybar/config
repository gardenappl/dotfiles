{
	"layer": "top",
	"position": "top",
	"height": 32,
	"modules-left": ["tray", "hyprland/workspaces", "sway/workspaces", "sway/scratchpad", "sway/mode", "hyprland/submap"],
	"modules-center": ["hyprland/window", "sway/window"],
	"modules-right": ["gamemode", "cpu", "temperature", "idle_inhibitor", "custom/hyprland/touch", "custom/sway/touch", "custom/alsa", "pulseaudio", "sway/language", "hyprland/language", "network", "battery", "clock"],
	"output": "!HEADLESS-1",
	"wlr/workspaces": {
		"disable-scroll": true,
		"all-outputs": true,
		"format": "{icon}",
		"sort-by-number": true,
		"format-icons": {
			"1": "1",
			"2": "2",
			"3": "3",
			"4": "4",
			"5": "5",
			"6": "6",
			"7": "7",
			"8": "8",
			"9": "9",
			"10": "0"
		}
	},
	"sway/mode": {
		"format": "<span style=\"italic\">{}</span>"
	},
	//"mpd": {
	//	"format": "{stateIcon} {consumeIcon}{randomIcon}{repeatIcon}{singleIcon}{artist} - {album} - {title} ({elapsedTime:%M:%S}/{totalTime:%M:%S}) ",
	//	"format-disconnected": "Disconnected ",
	//	"format-stopped": "{consumeIcon}{randomIcon}{repeatIcon}{singleIcon}Stopped ",
	//	"unknown-tag": "N/A",
	//	"interval": 2,
	//	"consume-icons": {
	//		"on": " "
	//	},
	//	"random-icons": {
	//		"off": "<span color=\"#f53c3c\"></span> ",
	//		"on": " "
	//	},
	//	"repeat-icons": {
	//		"on": " "
	//	},
	//	"single-icons": {
	//		"on": "1 "
	//	},
	//	"state-icons": {
	//		"paused": "",
	//		"playing": ""
	//	},
	//	"tooltip-format": "MPD (connected)",
	//	"tooltip-format-disconnected": "MPD (disconnected)"
	//},
	"idle_inhibitor": {
		"format": "<span font='icon'>{icon}</span>",
		"format-icons": {
			"activated": "👁",
			"deactivated": "⌛"
		},
		"tooltip-format-activated": "Idle inhibitor active",
		"tooltip-format-deactivated": "Idle inhibitor inactive"
	},
	"tray": {
		"icon-size": 20,
		"spacing": 10
	},
	"clock": {
		"tooltip-format": "{:%Y-%m-%d | %H:%M}",
		"format-alt": "{:%Y-%m-%d}"
	},
	"temperature": {
		"critical-threshold": 80,
		"format": "{temperatureC}°C<span font='icon'>{icon}</span>",
		"format-icons": ["🌡"]
	},
	"battery": {
		"states": {
			"warning": 30,
			"critical": 15
		},
		"format": "{capacity}% <span font='icon'>{icon}</span>",
		"format-charging": "{capacity}% <span font='icon'>🔌</span>",
		"format-plugged": "",
		"format-alt": "{time} <span font='icon'>{icon}</span>",
		"format-full": "",
		"format-icons": ["🪫", "🔋"]
	},
	"battery#bat2": {
		"bat": "BAT2"
	},
	"network": {
		"format-wifi": "{essid} <span font='icon'>🛜</span>",
		"format-ethernet": "{ipaddr}/{cidr} <span font='icon'>{icon}</span>",
		"format-linked": "{ifname} (No IP) <span font='icon'>⚠</span>",
		"format-disconnected": "Disconnected <span font='icon'>⚠</span>",
		"format-icons": ["🌐"],
		"tooltip-format": "{bandwidthUpBits} up, {bandwidthDownBits} down",
		"on-click": "iwctl-menu",
		"interval": 1
	},
	"pulseaudio": {
		// "scroll-step": 1, // %, can be a float
		"format": "{volume}% <span font='Noto Emoji'>{icon}</span>{format_source}",
		"format-bluetooth": "{volume}% <span font='icon'>{icon}🦷</span>{format_source}",
		"format-muted": "<span font='icon'>🔇</span>{format_source}",
		"format-source": " {volume}% <span font='icon'>🎤</span>",
		"format-source-muted": "",
		"format-icons": {
			"headphones": "🎧",
			"default": ["🔈", "🔉", "🔊"]
		},
		"on-click": "kitty -e pulsemixer"
	},
	"custom/sway/touch": {
		"format": "{}",
		"return-type": "json",
		"exec": "$HOME/.config/waybar/sway/touch",
		"exec-if": "swaymsg",
		"on-click": "sway-touch-toggle"
	},
	"custom/hyprland/touch": {
		"format": "{}",
		"return-type": "json",
		"exec": "$HOME/.config/waybar/hyprland/touch",
		"exec-if": "hypr-check",
		"interval": "once",
		"signal": 2,
		"on-click": "hypr-touch-toggle",
	},
	"custom/alsa": {
		"format": "{}",
		"interval": "once",
		"exec": "$HOME/.config/waybar/alsa",
		"exec-if": "! pamixer -v",
		"signal": 1
	},
	"sway/scratchpad": {
		"format": "<span font='icon'>{icon}</span> {count}",
		"show-empty": false,
		"format-icons": ["", "🗒️"],
		"tooltip": true,
		"tooltip-format": "{app}: {title}"
	},
	"sway/window": {
		"format": "{title}",
		"format-tooltip": "[{shell}] {appid}",
		"icon": true
	},
	"hyprland/window": {
		"icon": true,
		"rewrite": {
			"(.*) — Mozilla Firefox": "$1",
			"I2P Browser(.*)": "<span font='icon'>🕶️ $1</span>",
			"I2P браузер(.*)": "<span font='icon'>🕶️ $1</span>",
			"Bittorrent(.*)": "<span font='icon'>🧲 $1</span>",
			"Консоль(.*)": "<span font='icon'>💻 $1</span>"
		}
	},
	"hyprland/language": {
		"format": "{}",
		"format-en": "Eng󠁧󠁢󠁥󠁮󠁧󠁿",
		"format-ru": "Rus",
		"format-uk": "Ukr",
	},
	"gamemode": {
		"icon-spacing": 0,
		"icon-size": 18
	},
	"cpu": {
		"format": "{avg_frequency} GHz {icon0}{icon1}{icon2}{icon3}",
		"interval": 1,
		"format-icons": ["▁", "▂", "▃", "▄", "▅", "▆", "▇", "█"]
	}
}
