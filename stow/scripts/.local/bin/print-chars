#!/bin/bash

if [ -z "$1" ]; then
    echo "Usage: print-chars FONT [COLUMNS]"
    echo "    or print-chars --kitty-conf FONT"
elif [ "$1" = '--kitty-conf' ]; then
    printf 'symbol_map '
    fc-match "$2" --format '%{charset}' | sed 's/^/U+/; s/ /,U+/g; s/-/-U+/g'
    printf ' %s' "$2"
else
    columns="$2"
    if [ -z "$columns" ]; then
        columns=10
    fi
    # https://stackoverflow.com/a/60475015
    for range in $(fc-match --format='%{charset}\n' "$1"); do
        for n in $(seq "0x${range%-*}" "0x${range#*-}"); do
            printf "%04x\n" "$n"
        done
    done | while read -r n_hex; do
        count=$((count + 1))
        printf "%-5s\U$n_hex\t" "$n_hex"
        [ $((count % columns)) = 0 ] && printf "\n"
    done
    printf "\n"
fi
