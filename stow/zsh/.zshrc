#
# Minimal-ish Zsh config to bring in some features from Fish shell.
#

setopt printexitvalue

setopt CORRECT

HISTFILE=~/.cache/zsh_history
SAVEHIST=5000
HISTSIZE=2000

autoload -U colors && colors


# Prompt:
# [VI_MODE] username@hostname cwd % (git-branch)
setopt prompt_subst
base_prompt='%F{magenta}%n%f@%m %2~ $vcs_info_msg_0_%# '

autoload -Uz vcs_info
precmd_functions+=( vcs_info )
zstyle ':vcs_info:git:*' formats '%F{blue}(%b)%f '
zstyle ':vcs_info:*' enable git

function zle-line-init zle-keymap-select {
	case "$KEYMAP" in
		vicmd )
			vi_mode='%F{red}[N]%f'
			printf '\033[1 q' # block cursor
			;;
		* )
			vi_mode='%F{green}[I]%f'
			printf '\033[5 q' # beam cursor
			;;
	esac
	# Show current vi mode in bold
	PROMPT="%B$vi_mode%b $base_prompt"
	zle reset-prompt
}
zle -N zle-line-init
zle -N zle-keymap-select


# History substring search (like fish shell)

bindkey '^[[A' up-line-or-search
bindkey '^[[B' down-line-or-search
bindkey -M vicmd 'k' up-line-or-search
bindkey -M vicmd 'j' down-line-or-search


# Completion
# The following lines were added by compinstall

zstyle ':completion:*' completer _expand _complete _ignored _approximate
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
# case-insensitive, allow completion from the middle of a word
zstyle ':completion:*' matcher-list '' 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}' '+l:|=* r:|=*'
zstyle ':completion:*' menu select=1
zstyle :compinstall filename ~/.zshrc

autoload -Uz compinit
compinit
# End of lines added by compinstall


alias ls='ls -l --si --literal --group-directories-first --color=auto'

function ljq() {
	jq --color-output < "$1" | less -R
}

function lls() {
	ls --color=always $@ | less -R
}


#
# Plugin configs start here
#


# Standard plugins from Arch repos
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.plugin.zsh

# Plugin from AUR (abbreviations are just too good!)
source /usr/share/zsh/plugins/zsh-abbr/zsh-abbr.plugin.zsh

# Syntax highlighting for abbreviations
ZSH_HIGHLIGHT_REGEXP+=('^[[:blank:][:space:]]*('${(j:|:)${(Qk)ABBR_REGULAR_USER_ABBREVIATIONS}}')$' 'fg=magenta')
ZSH_HIGHLIGHT_REGEXP+=('\<('${(j:|:)${(Qk)ABBR_GLOBAL_USER_ABBREVIATIONS}}')$' 'fg=magenta')


# Should only be run once on system install, or when adding new abbreviations
function setup-abbr() {
	# Erase all currsent abbreviations
	for old_abbr in $(abbr | cut -d'=' -f1); do
		abbr e "$old_abbr"
	done

	abbr ga='git add'
	abbr --force gs='git status'
	abbr --force gc='git commit'
	abbr gp='git push'
	abbr gd='git diff'
	abbr gl='git log'
	abbr gf='git fetch'
	abbr hypr='Hyprland'
	abbr tre='tremc -X'
	abbr sp='sudo pacman'
	abbr sc='systemctl'
	abbr ssc='sudo systemctl'
	abbr y='LESS=SR yay'
	abbr j4='j4-dmenu-desktop --term="kitty --single-instance"'
	abbr stp='sudo htop'
	abbr f='df -h'

	abbr --force vim='nvim'
	abbr --force ncdu='ncdu -x --confirm-quit'
	abbr --force radeontop='sudo radeontop --color'
	abbr --force grep='grep --color=auto'
	abbr --force diff='diff --color=auto'
	abbr --force df='df -h'

	abbr W='WINEPREFIX=~/wine/'
	abbr DJ='DISPLAY= j4-dmenu-desktop'

	abbr -g ws='wine start'
	abbr -g wk='wineserver -k'
	abbr -g lessr='less -R'

	abbr mpvm='mpv --audio-channels=mono'
	abbr smw='sudo mount -o gid=wheel,fmask=113,dmask=002'
	abbr sumo='sudo umount'
	abbr upkg='updpkgsums && makepkg --printsrcinfo > .SRCINFO'
}
