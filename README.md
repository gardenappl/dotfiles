# gardenapple/dotfiles

> [!WARNING]
> I don't really update these anymore, I've moved on to using KDE Plasma 6, which I'm fairly happy with. I may still update some scripts, Neovim, and other minor tweaks, but [I will not be using Hyprland anymore.](https://drewdevault.com/2023/09/17/Hyprland-toxicity.html)

These are some of my Unix-y dotfiles. There isn't too much customization, but there are a few notable things:

1. **I use Wayland when I can**, ~~my setup is built around [hyprland.org](https://hyprland.org/)~~ and pretty much everything is configured to run on native Wayland.
2. **I prefer white themes, even for the terminal.** [Black text on a light background is easier to read!](https://ux.stackexchange.com/questions/53264/dark-or-white-color-theme-is-better-for-the-eyes) If you insist that white themes make your eyes hurt, I highly recommend installing [wlsunset](https://sr.ht/~kennylevinsen/wlsunset/) and/or taking breaks occasionally.

These dotfiles are managed using [GNU Stow](https://www.gnu.org/software/stow/). To install them, run the following commands:

```
git clone https://gitlab.com/gardenappl/dotfiles.git
cd dotfiles/stow
stow * --target ~
```

This should work on pretty much any system as long as you have git and Stow installed.

### Configured programs

* [avizo](https://github.com/misterdanb/avizo) - notification daemon
* elinks - terminal web browser, more of a gimmick
* fontconfig
* htop - process manager
* Hyprland - window manager
* kitty - terminal
* lf - file manager
* mailcap - file opening config
* mako - notification manager for Wayland
* newsboat - RSS reader
* neovim - text editor
* readline - Vim keys for many interactive CLIs
* [tg](https://github.com/paul-nameless/tg) - Telegram client
* user-dirs - set XDG dirs
* vieb - keyboard-driven web browser
* waybar - status bar
* yay - AUR helper
* yt-dlp - YouTube (and other sites) video/audio downloader, integrates nicely with `mpv`
* zsh - configured to behave like Fish shell with just a few plugins
  * zsh-abbr (AUR)
  * zsh-autosuggestions
  * zsh-syntax-highlighting

Various Creative Commons wallpapers are available in `stow/wallpapers`.

Other things that these dotfiles rely on: 
* aerc - mail client
* amfora - Gemini client
* bc - calculator language
* bemenu (`/usr/bin/dmenu` is symlinked to `/usr/bin/bemenu`)
* [ctpv](https://github.com/NikitaIvanovV/ctpv) - image and file previews for `lf` file manager
* DejaVu Sans, Linertinus Serif, Font Awesome
* firefox
* i2p
* j4-dmenu-desktop 
* LBRY
* [lbt](https://gitlab.com/gardenappl/lbt)
* mdcat - Markdown previews
* mpv - video/audio player
* pulsemixer, pamixer **or** amixer
* slurp, grim - taking screenshots
* swaybg, swayidle, swaylock
* unicode-emoji
* urlscan
* [vivid](https://github.com/sharkdp/vivid) - provides LS_COLORS
* wl-clipboard
* wlsunset - Wayland gamma control
* xdg-open
* Dependencies for nvim plugins: cmake, python-pynvim, \*-language-server

And probably some others that I'm forgetting right now.

### Scripts

[.local/bin](./stow/scripts/.local/bin) contains some tiny scripts which some might find useful.
